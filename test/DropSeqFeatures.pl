#!/usr/bin/perl

use warnings;
use strict;
use Bio::DB::Sam;
use Bio::DB::HTS::Tabix;

sub ParseDGE($$);
sub ParseBam($$$$);

MAIN:
{
    my $file_dge = shift;
    my $file_bam = shift;
    my $file_tbi = shift;
    my $threshold = 500;
    my %cells = ();
    
    
    
    ParseDGE($file_dge, \%cells);
    
    # open file pointer
    my $bam = Bio::DB::Bam->open($file_bam);
    my $tabix = Bio::DB::HTS::Tabix->new(filename => $file_tbi);
    
    ParseBam($bam, $tabix, \%cells, $threshold);
    
    # close file pointers
    #$bam->close;
    $tabix->close;
    
    exit 0;
}


### Parse Bam
sub ParseBam($$$$)
{
    my $bam = $_[0];
    my $tabix = $_[1];
    my $cells_ref = $_[2];
    my $threshold = $_[3];
    my %stats = ();
    
    my $header = $bam->header;
    my $target_count = $header->n_targets;
    my $target_names = $header->target_name;
    my $line_all = 0;
    my $line_unq = 0;
    my $line_cid = 0;
    while (my $align = $bam->read1)
    {
        ### increment all lines
        $line_all++;
        
        ### filter for unique alignments only
        my $qual = $align->qual;
        next if($qual != 255);
        $line_unq++;
        
        ### filter for genes per cell
        my $cellid = $align->get_tag_values('XC');
        my $genes = exists($cells_ref->{$cellid}) ? $cells_ref->{$cellid} : 0;
        next if($genes < $threshold);
        
        ### find annotation
        my $seqid = $target_names->[$align->tid];
        my $start = $align->pos;
        my $end = $align->calend;
        my $strand = $align->strand;
        my $query = $seqid . ':' . $start . '-' . $end;
        #print $query,"\n";
        my $iter = $tabix->query("$seqid:$start-$end");
        my $feature = "intergenic";
        
            if(my $record = $iter->next)
            {
                #$feature = ($record =~ m/;([53A-Za-z\_]+)\s/) ? $1 : "unknown";
                my $ps = rindex($record, ';') + 1;
                my $pe = index($record, "\t", $ps);
                
                $feature = substr($record,$ps, $pe - $ps);
                
                #print $feature,"\t",$record,"\t",$ps,"\t",$pe,"\n";
            }
        $stats{$feature}++;
        $line_cid++;
        #last if($line_cid == 10000);
        
    }
    
    print "lines.all\t",$line_all,"\n";
    print "lines.unq\t",$line_unq,"\n";
    print "lines.cid\t",$line_cid,"\n";
    
    foreach my $key (keys %stats)
    {
        #print $key,"\t",$stats{$key},"\t",sprintf("%.2f",100*$stats{$key}/$line),"\n";
        print $key,"\t",$stats{$key},"\n";
    }

    
}


## Parse DGE
sub ParseDGE($$)
{
    my $file_dge = $_[0];
    my $cells_ref = $_[1];
    
    open(my $fh, "<", $file_dge) or die $!;
    while(<$fh>)
    {
        chomp($_);
        next if($_ =~ m/^$/);
        next if($_ =~ m/^#/);
        next if($_ =~ m/CELL_BARCODE/);
        
        my($cellid, $genes, $rest) = split("\t", $_, 3);
        $cells_ref->{$cellid} = $genes;
        
    }
    close($fh);
}


