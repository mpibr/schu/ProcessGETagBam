#!/usr/bin/perl

use warnings;
use strict;

sub ParseStats($$);
sub PrintTable($);

MAIN:
{
    my $file_stats = shift;
    my %hdata = ();

    ParseStats($file_stats, \%hdata);
    PrintTable(\%hdata);

    exit 0;
}

sub PrintTable($)
{
    my $hdata_ref = $_[0];
    my @header = ("lines.all",
    "lines.unq",
    "lines.cid",
    "5pUTR",
    "5pUTR_extended",
    "CDS",
    "CDS_extended",
    "3pUTR",
    "3pUTR_extended",
    "intron",
    "intergenic");
    
    print "#species\tsamples\t",join("\t",@header),"\n";
    foreach my $species (keys %{$hdata_ref})
    {
        foreach my $sample (keys %{$hdata_ref->{$species}})
        {
            print $species,"\t",$sample;
            foreach(@header)
            {
                my $count = exists($hdata_ref->{$species}{$sample}{$_}) ? $hdata_ref->{$species}{$sample}{$_} : 0;
                print "\t",$count;
            }
            print "\n";
        }
    }
}

sub ParseStats($$)
{
    my $file_stats = $_[0];
    my $hdata_ref = $_[1];
    
    open(my $fh, "<", $file_stats) or die $!;
    while(<$fh>)
    {
        chomp($_);
        my ($species, $sample, $feature, $counts) = split("\t", $_, 4);
        $hdata_ref->{$species}{$sample}{$feature} = $counts;
    }
    close($fh);
    
    return;
}




