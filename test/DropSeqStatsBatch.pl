#!/usr/bin/perl

use warnings;
use strict;

sub getSubPathList($$);

MAIN:
{
    my $command = shift;
    my $path_data = shift;
    my $ann_lizard = shift;
    my $ann_turtle = shift;
    my @subpath = ();
    
    my $threshold_lizard = 500;
    my $threshold_turtle = 800;
    my $mapq = 255;
    my %hdata = ();
    my %href = ();
    
    getSubPathList($path_data, \@subpath);
    
    foreach my $currpath (@subpath)
    {
        # set species
        my $species = "<undef>";
        $species = "lizard" if($currpath =~ m/lizard/);
        $species = "turtle" if($currpath =~ m/turtle/);
        $hdata{$currpath}{"species"} = $species;
        
        # set species specific parameters
        my $ann = "<undef>";
        my $threshold = "<undef>";
        if ($species eq "lizard")
        {
            $ann = $ann_lizard;
            $threshold = $threshold_lizard;
        }
        elsif ($species eq "turtle")
        {
            $ann = $ann_turtle;
            $threshold = $threshold_turtle;
        }
        
        # extract current BAM and DGE files
        my $currFullPath = $path_data . '/' . $currpath;
        my @bamFiles = glob($currFullPath .'/'. '*GEtagged.bam');
        my @dgeFiles = glob($currFullPath .'/'. '*summary.txt');
        #print $bamFiles[0],"\n";
        #print $txtFiles[0],"\n";
        
        # command
        $command .= " --mapq " . $mapq . " --threshold " . $threshold . " --bam " . $bamFiles[0] . " --dge " . $dgeFiles[0] . " --annotation " . $ann;
        
        open(my $fh, '-|', $command) or die $!;
        while(<$fh>)
        {
            chomp($_);
            my ($key, $value) = split("\t", $_, 2);
            
            print $species,"\t",$currpath,"\t",$key,"\t",$value,"\n";
            
            $hdata{$currpath}{"stats"}{$key} = $value;
            $href{$key}++;
            
        }
        close($fh);
        
        
        # write out stats
        
        
        
        
        
        
        #last;
        
        
        
    }
    
    
    
    exit 0;
}

sub getSubPathList($$)
{
    my $path_data = $_[0];
    my $subpath_ref = $_[1];
    opendir(my $dh, $path_data) or die $!;
    while (readdir $dh)
    {
        next if(-f $path_data . '/' . $_);
        next if($_ =~ m/^\./);
        
        push(@{$subpath_ref}, $_);
    }
    closedir($dh);
    
    return;
}






