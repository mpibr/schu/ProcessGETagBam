//
//  main.c
//  ProcessGETagBam
//
//  Created by Georgi Tushev on 29/09/17.
//  Copyright © 2017 Scientific Computing. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include "config.h"
#include "htsutils.h"

int main(int argc, const char * argv[])
{
    aux_config_t aux;
    hts_config_t hts;
    
    clock_t begin = clock();
    
    // parse auxiliary configuration
    aux_init(&aux);
    if (aux_parse(&aux, argc, argv))
    {
        help();
        return EXIT_FAILURE;
    }
    
    // initialize with hts
    hts_init(&hts, &aux);
    
    // parse dge
    hts_parse_dge(&hts);
    
    
    // parse bam
    hts_parse_bam(&hts);
    
    // dump hash
    hts_dumphash(hts.kh_ftr);
    
    
    // free
    hts_destroy(&hts);
    
    
    clock_t end = clock();
    
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    
    fprintf(stderr, "Time: %.4f sec\n", time_spent);
    
    return 0;
}
