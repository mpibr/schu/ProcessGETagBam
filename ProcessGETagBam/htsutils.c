//
//  htsutils.c
//  ProcessGETagBam
//
//  Created by Georgi Tushev on 30/09/17.
//  Copyright © 2017 Scientific Computing. All rights reserved.
//

#include "htsutils.h"


void hts_init(hts_config_t *hts, aux_config_t *aux)
{
    /* open DGE file */
    hts->fh_cells = fopen(aux->file_dge, "r");
    if (!hts->fh_cells)
    {
        fprintf(stderr, "Error : HTS_INIT : coult not open input stream from %s\n", aux->file_dge);
        exit(EXIT_FAILURE);
    }
    
    /* open BAM file */
    hts->fh_bam = sam_open(aux->file_bam, "r");
    if (!hts->fh_bam)
    {
        fprintf(stderr, "Error : HTS_INIT :  coult not open BAM stream from %s\n", aux->file_bam);
        exit(EXIT_FAILURE);
    }
    
    /* parse BAM header */
    hts->bam_header = sam_hdr_read(hts->fh_bam);
    if (!hts->bam_header)
    {
        fprintf(stderr, "Error : HTS_INIT : coult not read BAM header %s\n", aux->file_bam);
        exit(EXIT_FAILURE);
    }
    
    /* read tabix index */
    hts->tabix = regidx_init(aux->file_annotation, custom_parse_bed, custom_free, sizeof(char *), NULL);
    if (!hts->tabix)
    {
        fprintf(stderr, "Error : HTS_INIT : failed to load annotation %s\n", aux->file_annotation);
        exit(EXIT_FAILURE);
    }
    
    /* initialize BAM record */
    hts->bam = bam_init1();
    
    /* initialize hash map */
    hts->kh_dge = kh_init(hmap);
    hts->kh_ftr = kh_init(hmap);
    
    /* set thresholds */
    hts->threshold = aux->threshold;
    hts->mapq = aux->mapq;
    
    return;
}


void hts_parse_dge(hts_config_t *hts)
{
    /* parsing related variables */
    const int buffer_size = 512;
    char buffer[buffer_size];
    int lines = 0;
    
    /* hash related variables */
    int absent;
    khiter_t hiter;
    char hkey[16];
    uint32_t hvalue;
    
    while (fgets(buffer, buffer_size, hts->fh_cells) != 0)
    {
        // remove header or empty lines
        if ((buffer[0] == '\0') || (buffer[0] == '#') || (buffer[0] == '\n'))
            continue;
        
        // remove meta data
        if (strncmp(buffer, "CELL_BARCODE", 12) == 0)
            continue;
        
        // read cell barcode and gene count
        sscanf(buffer,"%s %d %*s",hkey, &hvalue);
        
        // fill hash
        hiter = kh_put(hmap, hts->kh_dge, hkey, &absent);
        if (absent)
        {
            kh_key(hts->kh_dge, hiter) = strdup(hkey);
            kh_value(hts->kh_dge, hiter) = hvalue;
        }
        
        lines++;
        
    }
    //printf("DGE lines :: %d\n", lines);
    
    return;
}


void hts_parse_bam(hts_config_t *hts)
{
    uint32_t lines_all = 0;
    uint32_t lines_unq = 0;
    uint32_t lines_cid = 0;
    //uint32_t lines_err = 0;
    
    char *chrom;
    uint32_t chrom_start;
    uint32_t chrom_end;
    regitr_t ritr;
    
    // hash related variables
    int absent;
    khiter_t hiter;
    
    // read BAM record by record
    while (sam_read1(hts->fh_bam, hts->bam_header, hts->bam) >= 0)
    {
        // increment all lines
        lines_all++;
        
        // filter based on MAPQ
        if (hts->bam->core.qual < hts->mapq)
            continue;
        
        // increment unq lines
        lines_unq++;
        
        // return tag
        uint8_t *tag = bam_aux_get(hts->bam, "XC");
        char *hkey = bam_aux2Z(tag);
        
        // check hash map
        hiter = kh_get(hmap, hts->kh_dge, hkey);
        uint32_t genes = 0;
        if (hiter != kh_end(hts->kh_dge))
        {
            genes = kh_value(hts->kh_dge, hiter);
        }
        if (genes < hts->threshold)
            continue;
        
        // set tabix query
        chrom = hts->bam_header->target_name[hts->bam->core.tid];
        chrom_start = hts->bam->core.pos + 1;
        chrom_end = bam_calend(&hts->bam->core, bam_get_cigar(hts->bam));
        if (hts->bam->core.flag & BAM_FREVERSE)
        {
            chrom_end = chrom_start + 1;
        }
        else
        {
            chrom_start = chrom_end - 1;
        }
        
        
        char *fkey = "intergenic";
        if (regidx_overlap(hts->tabix, chrom, chrom_start, chrom_end, &ritr))
        {
            fkey = REGITR_PAYLOAD(ritr, char*);
            
            /*
            int n = 0;
            while ( REGITR_OVERLAP(ritr,chrom_start,chrom_end) )
            {
                printf("%d [%d,%d] overlaps with [%d,%d], payload=%s\n", lines_unq, chrom_start,chrom_end, REGITR_START(ritr), REGITR_END(ritr), REGITR_PAYLOAD(ritr,char*));
                ritr.i++;
                n++;
            }
            
            if (n > 1)
            {
                lines_err++;
                break;
            }
            */
        }
        
        
        
        // fill feature hash
        hiter = kh_put(hmap, hts->kh_ftr, fkey, &absent);
        if (absent)
        {
            kh_key(hts->kh_ftr, hiter) = strdup(fkey);
            kh_value(hts->kh_ftr, hiter) = 1;
        }
        else
        {
            kh_value(hts->kh_ftr, hiter)++;
        }
        
        
        // count bam lines
        lines_cid++;
        
        //if (lines_cid == 1000)
        //    break;
    }
    
    printf("lines.all\t%d\n", lines_all);
    printf("lines.unq\t%d\n", lines_unq);
    printf("lines.cid\t%d\n", lines_cid);
    //printf("lines.err\t%d\n", lines_err);
    
    
    return;
}


void hts_freehash(khash_t(hmap) **href)
{
    khash_t(hmap) *hash = *href;
    khiter_t hitr;
    char *hkey;
    //uint32_t hvalue;
    
    if (hash == 0) return;
    
    for (hitr = kh_begin(hash); hitr != kh_end(hash); ++hitr)
    {
        if (kh_exist(hash, hitr))
        {
            // free key
            hkey = (char *)kh_key(hash, hitr);
            free(hkey);
        }
    }
    kh_destroy(hmap, hash);
    
    return;
}


void hts_dumphash(khash_t(hmap) *hash)
{
    khiter_t hiter;
    char *hkey;
    uint32_t hvalue;
    
    for (hiter = kh_begin(hash); hiter != kh_end(hash); ++hiter)
    {
        if (kh_exist(hash, hiter))
        {
            hkey = (char *)kh_key(hash, hiter);
            hvalue = kh_value(hash, hiter);
            printf("%s\t%d\n", hkey, hvalue);
        }
    }
    
    return;
}

void hts_destroy(hts_config_t *hts)
{
    hts_freehash(&hts->kh_dge);
    hts_freehash(&hts->kh_ftr);
    bam_destroy1(hts->bam);
    bam_hdr_destroy(hts->bam_header);
    regidx_destroy(hts->tabix);
    sam_close(hts->fh_bam);
    fclose(hts->fh_cells);
    return;
}


/* bam_calend
 * Calculate the rightmost coordinate of an alignment on the reference genome.
 * bam.h https://github.com/samtools/samtools/search?utf8=%E2%9C%93&q=bam_calend
 */

static inline uint32_t bam_calend(const bam1_core_t *core, const uint32_t *cigar)
{
    return core->pos + (core->n_cigar ? bam_cigar2rlen(core->n_cigar, cigar) : 1);
}



int custom_parse_bed(const char *line, char **chr_beg, char **chr_end, reg_t *reg, void *payload, void *usr)
{
    char *ss = (char *) line;
    while (*ss && isspace(*ss)) ss++;
    if (!*ss) return -1; // skip blank lines
    if (*ss == '#') return -1; // skip comments
    
    char *se = ss;
    while (*se && (*se != '\t')) se++;
    if (!*se)
    {
        hts_log_error("Could not parse bed line: %s", line);
        return -2;
    }
    *chr_beg = ss;
    *chr_end = se - 1;
    
    ss = se + 1;
    reg->start = (uint32_t)hts_parse_decimal(ss, &se, 0);
    if (ss == se)
    {
        hts_log_error("Could not parse bed line: %s", line);
        return -2;
    }
    
    ss = se + 1;
    reg->end = (uint32_t)hts_parse_decimal(ss, &se, 0) - 1;
    if ( ss==se )
    {
        hts_log_error("Could not parse bed line: %s", line);
        return -2;
    }
    
    se++;
    ss = se;
    while (*se && (*se != '\t'))
    {
        if(*se == ';')
        {
            ss = se + 1;
        }
        se++;
    }
    if (!*se)
    {
        hts_log_error("Could not parse bed line: %s", line);
        return -2;
    }
    
    
    char **dat = (char **)payload;
    *dat = (char *)malloc(sizeof(char)*(se - ss + 1));
    memcpy(*dat, ss, se-ss+1);
    (*dat)[se-ss] = '\0';
    
    return 0;
}

void custom_free(void *payload)
{
    char **dat = (char**)payload;
    free(*dat);
}
