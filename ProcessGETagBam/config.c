//
//  config.c
//  ProcessGETagBam
//
//  Created by Georgi Tushev on 29/09/17.
//  Copyright © 2017 Scientific Computing. All rights reserved.
//

#include "config.h"

const char VERSION[] = "v0.01";
const char PROGRAM_NAME[] = "ProcessGETagBam";

void aux_init(aux_config_t *aux)
{
    /* initialize default auxiliary values */
    aux->file_bam = NULL;
    aux->file_annotation = NULL;
    aux->file_dge = NULL;
    aux->threshold = AUX_DEFAULT_THRESHOLD;
    aux->mapq = AUX_DEFAULT_MAPQ;
    
    return;
}

int aux_parse(aux_config_t *aux, int argc, const char *argv[])
{
    /* parse input pairs */
    for(int i = 1; i < argc; i++)
    {
        // current paramter length
        uint64_t parameterLength = strlen(argv[i]);
        
        // check each parameter and its value
        if((PARAMETER_CHECK("-h", 2, parameterLength)) ||
           (PARAMETER_CHECK("--help", 6, parameterLength)))
        {
            help();
        }
        else if((PARAMETER_CHECK("-v", 2, parameterLength)) ||
                (PARAMETER_CHECK("--version", 9, parameterLength)))
        {
            version();
        }
        else if((PARAMETER_CHECK("-q", 2, parameterLength)) ||
                (PARAMETER_CHECK("--mapq", 6, parameterLength)))
        {
            i += 1;
            aux->mapq = (uint32_t)strtoul(argv[i], NULL, 10);
        }
        else if((PARAMETER_CHECK("-t", 2, parameterLength)) ||
                (PARAMETER_CHECK("--threshold", 11, parameterLength)))
        {
            i += 1;
            aux->threshold = (uint32_t)strtoul(argv[i], NULL, 10);
        }
        else if((PARAMETER_CHECK("-b", 2, parameterLength)) ||
                (PARAMETER_CHECK("--bam", 5, parameterLength)))
        {
            i += 1;
            aux->file_bam = argv[i];
        }
        else if((PARAMETER_CHECK("-a", 2, parameterLength)) ||
                (PARAMETER_CHECK("--annotation", 12, parameterLength)))
        {
            i += 1;
            aux->file_annotation = argv[i];
        }
        else if((PARAMETER_CHECK("-d", 2, parameterLength)) ||
                (PARAMETER_CHECK("--dge", 5, parameterLength)))
        {
            i += 1;
            aux->file_dge = argv[i];
        }
        else
        {
            fprintf(stderr, "Error:: Unknown parameter %s\n", argv[i]);
            return EXIT_FAILURE;
        }
        
    }
    
    return EXIT_SUCCESS;
}



void version(void)
{
    fprintf(stderr, "%s %s\n", PROGRAM_NAME, VERSION);
    return;
}

void help(void)
{
    
    fprintf(stderr, "\nTool: %s\n", PROGRAM_NAME);
    fprintf(stderr, "Version: %s\n", VERSION);
    fprintf(stderr, "Summary:\n");
    fprintf(stderr, "\tFilter alignments based on cell ID.\n");
    fprintf(stderr, "\nUsage:\t%s [OPTIONS] -b/--bam <bam/stdin> -a/--annotation <bed>\n", PROGRAM_NAME);
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "\t-b|--bam BAM/stdin file\n");
    fprintf(stderr, "\t-a|--annotation BED file with annotated regions\n");
    fprintf(stderr, "\t-d|--dge DGE txt file with CellIDs and gene count\n");
    fprintf(stderr, "\t-t|--threshold\tminimum number of genes per cell to keep alignment\n");
    fprintf(stderr, "\t-q|--mapq\talignment score filter\n");
    fprintf(stderr, "\t-h|--help\tprint help message\n");
    fprintf(stderr, "\t-v|--version\tprint current version\n");
    return;
}

int chomp(char *line_buf)
{
    size_t  line_length;
    line_length = strlen(line_buf) - 1;
    if(line_buf[line_length] == '\n')
    line_buf[line_length] = '\0';
    
    return 0;
}