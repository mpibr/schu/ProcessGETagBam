//
//  htsutils.h
//  ProcessGETagBam
//
//  Created by Georgi Tushev on 30/09/17.
//  Copyright © 2017 Scientific Computing. All rights reserved.
//

#ifndef htsutils_h
#define htsutils_h

#include <stdio.h>
#include <ctype.h>

#include <htslib/sam.h>
#include <htslib/regidx.h>
#include <htslib/khash.h>


#include "config.h"

KHASH_MAP_INIT_STR(hmap, uint32_t);


typedef struct _hts_config_t
{
    samFile *fh_bam;
    bam_hdr_t *bam_header;
    regidx_t *tabix;
    bam1_t *bam;
    FILE *fh_cells;
    khash_t(hmap) *kh_dge;
    khash_t(hmap) *kh_ftr;
    uint32_t threshold;
    uint32_t mapq;
} hts_config_t;

void hts_init(hts_config_t *, aux_config_t *);
void hts_parse_dge(hts_config_t *);
void hts_parse_bam(hts_config_t *);
void hts_freehash(khash_t(hmap) **);
void hts_dumphash(khash_t(hmap) *);
void hts_destroy(hts_config_t *);

static inline uint32_t bam_calend(const bam1_core_t *, const uint32_t *);

int custom_parse_bed(const char *line, char **chr_beg, char **chr_end, reg_t *reg, void *payload, void *usr);
void custom_free(void *payload);

#endif /* htsutils_h */
