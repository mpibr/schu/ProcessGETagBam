//
//  config.h
//  ProcessGETagBam
//
//  Created by Georgi Tushev on 29/09/17.
//  Copyright © 2017 Scientific Computing. All rights reserved.
//

#ifndef config_h
#define config_h

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// define some constants
#define MAX_LENGTH 1024
#define AUX_DEFAULT_THRESHOLD 0
#define AUX_DEFAULT_MAPQ 0

// define MIN & MAX macros
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

// define our parameter checking macro
#define PARAMETER_CHECK(param, paramLen, actualLen) (strncmp(argv[i], param, MIN(actualLen, paramLen))== 0) && (actualLen == paramLen)


// define parameters
typedef struct _aux_config_t
{
    const char    *file_bam;
    const char    *file_annotation;
    const char    *file_dge;
    uint32_t threshold;
    uint32_t mapq;
} aux_config_t;

// function declarations
void aux_init(aux_config_t *);
int aux_parse(aux_config_t *, int, const char **);
void help(void);
void version(void);
int chomp(char *);

#endif /* config_h */
